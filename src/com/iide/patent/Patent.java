/*
 *	Patent class by Andrew 
 */

package com.iide.patent;

public class Patent {

	private String id;
	private String username;
	private String title;
	private String status;
	private String xml;
	private String submittedDate;
	private String publishedDate;
	private String modifiedDate;

	public Patent(String id, String username, String title, String status,
			String xml, String submittedDate, String publishedDate,
			String modifiedDate) {
		this.id = id;
		this.username = username;
		this.title = title;
		this.status = status;
		this.xml = xml;
		this.submittedDate = submittedDate;
		this.publishedDate = publishedDate;
		this.modifiedDate = modifiedDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getXml() {
		return xml;
	}

	public void setXml(String xml) {
		this.xml = xml;
	}

	public String getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(String submittedDate) {
		this.submittedDate = submittedDate;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getId() {
		return id;
	}

}
