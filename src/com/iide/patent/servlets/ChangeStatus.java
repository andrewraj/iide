/*
 * Change status servlet by Andrew
 */

package com.iide.patent.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iide.patent.Database;
import com.iide.patent.PatentStatus;

@WebServlet("/ChangeStatus")
public class ChangeStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Database db = new Database(request);

		String status = request.getParameter("status");
		String id = request.getParameter("id");

		if (status.equals("publish")) {
			db.updatePatent(id, PatentStatus.PUBLISHED);
			response.sendRedirect("reviewer.jsp");
			return;
		} else if (status.equals("reject")) {
			db.updatePatent(id, PatentStatus.REJECTED);
			response.sendRedirect("reviewer.jsp");
			return;
		}
		response.sendRedirect("reviewer.jsp");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
