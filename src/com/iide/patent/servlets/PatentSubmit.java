/*
 * Patent submission servlet by Chanthu
 */

package com.iide.patent.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iide.patent.Database;
import com.iide.patent.SessionObject;

@WebServlet("/PatentSubmit")
public class PatentSubmit extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		Database db = new Database(request);
		SessionObject ses = SessionObject.get(request);

		String name = request.getParameter("name");
		if (name == null | name.equals("")) {
			response.sendRedirect("applicant.jsp?error=Please specify a name after uploading");
			return;
		}

		String path = request.getParameter("path");

		db.submitPatent(ses.getName(), name, path);
		response.sendRedirect("applicant.jsp");
	}

	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
