/*
 * Signin Signout servlet by Chanthu & Andrew
 */
package com.iide.patent.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.iide.patent.Database;
import com.iide.patent.SessionObject;
import com.iide.patent.User;

@SuppressWarnings("serial")
@WebServlet("/SigninSignoutServlet")
public class SigninSignoutServlet extends HttpServlet {

	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		SessionObject ses = SessionObject.get(req);

		Database db = new Database(req);

		String type = req.getParameter("type");

		if (type.equals("login")) {
			String username = req.getParameter("username");
			String password = req.getParameter("password");

			User user = db.getUser(username, password);
			if (user != null) {
				ses.setName(user.getUsername());
				ses.setUser(user);
			} else {
				res.sendRedirect(req.getParameter("returnUrl")
						+ "?error=Failed to login using the provided credentials.<br/>Try again.");
				return;
			}
		} else if (type.equals("signup")) {
			String username = req.getParameter("username");
			String password = req.getParameter("password");
			String fName = req.getParameter("firstname");
			String lName = req.getParameter("lastname");
			String email = req.getParameter("email");

			if (db.addUser(username, password, email, fName, lName)) {
				ses.setName(username);
				ses.setUser(new User(username, fName, lName, email, true));
			} else {
				res.sendRedirect(req.getParameter("returnUrl")
						+ "?error=Failed to sign up using the provided details.<br/>Try again.");
				return;
			}
		} else if (type.equals("logout")) {
			ses.setName(null);
		}
		res.sendRedirect(req.getParameter("returnUrl"));
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		doGet(req, res);
	}
}
