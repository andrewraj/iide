/*
 * PatentStatus enumerator by Chanthu
 */

package com.iide.patent;

public enum PatentStatus {
	PUBLISHED, REJECTED, PENDING
}
