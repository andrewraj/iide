/*
 * Session object class by Chanthu
 */

package com.iide.patent;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionObject {
	public static final String NAME = "ARandomObjectKey";

	private User user;
	private String name;

	private SessionObject() {
	}

	public boolean isApplciant() {
		return user.isApplciant();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public boolean isLoggedIn() {
		return name != null;
	}

	// ------------------ STATIC SESSION INITIALIZERS ------------------//
	public synchronized static SessionObject get(HttpServletRequest req) {
		return get(req.getSession());
	}

	public synchronized static SessionObject get(HttpSession ses) {
		SessionObject sesObj = null;
		if (ses.getAttribute(SessionObject.NAME) == null) {
			sesObj = new SessionObject();
			ses.setAttribute(SessionObject.NAME, sesObj);
		} else {
			Object sobj = ses.getAttribute(SessionObject.NAME);
			sesObj = (SessionObject) sobj;
		}

		return sesObj;
	}
}
