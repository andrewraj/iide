/*
 * Database class by Andrew & Chanthu
 */

package com.iide.patent;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;

public class Database {
	private static Connection c = null;

	private String path;

	public Database(String path) {
		this.path = path;
	}

	public Database(HttpServletRequest req, String path) {
		this.path = req.getServletContext().getRealPath(path);
	}

	public Database(HttpServletRequest req) {
		this.path = req.getServletContext().getRealPath("/database/data.db");
	}

	/**
	 * Adding a user to the database
	 * 
	 * @param username
	 * @param password
	 * @param email
	 * @param fName
	 * @param lName
	 * @return true if entry is successful
	 */
	public boolean addUser(String username, String password, String email,
			String fName, String lName) {
		Statement stmt = null;

		try {
			openDatabase();
			password = MD5Hashing.hashPassword(password);
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "INSERT INTO USER (USERNAME,PASSWORD,EMAIL,FIRST_NAME,LAST_NAME) "
					+ "VALUES ('"
					+ username
					+ "','"
					+ password
					+ "','"
					+ email
					+ "','" + fName + "', '" + lName + "' );";
			stmt.executeUpdate(sql);
			stmt.close();
			c.commit();
			closeDatabase();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Validates username and password 
	 * @param username
	 * @param password
	 * @return true if username and password is found in the database
	 */
	public boolean checkUsernameAndPassword(String username, String password) {
		Statement stmt = null;
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM USER;");
			while (rs.next()) {
				if (rs.getString("USERNAME").equals(username)) {
					password = MD5Hashing.hashPassword(password);
					if (rs.getString("PASSWORD").equals(password)) {
						rs.close();
						stmt.close();
						closeDatabase();
						return true;
					}
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Retrieves a user object from the database
	 * @param username
	 * @param password
	 * @return user object
	 */
	public User getUser(String username, String password) {
		User user = null;
		Statement stmt = null;
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM USER WHERE USERNAME='"
							+ username + "'");
			while (rs.next()) {
				password = MD5Hashing.hashPassword(password);
				if (rs.getString("PASSWORD").equals(password)) {

					String userN = rs.getString("USERNAME");
					String email = rs.getString("EMAIL");
					String fName = rs.getString("FIRST_NAME");
					String lName = rs.getString("LAST_NAME");
					String role = rs.getString("ROLE_ID");

					user = new User(userN, fName, lName, email,
							role.equals("ap"));

					rs.close();
					stmt.close();
					closeDatabase();

					return user;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Inserting a patent application to the database
	 * @param username
	 * @param xml
	 * @return true if entry is successful
	 */
	public boolean submitPatent(String username, String title, String xml) {
		Statement stmt = null;
		String applicationID = generateRandomApplicationID();
		java.util.Date today = new java.util.Date();
		String date = new Date(today.getTime()).toString();

		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "INSERT INTO APPLICATION (APPLICATION_ID,TITLE,XML,SUBMITTED_DATE) "
					+ "VALUES ('"
					+ applicationID
					+ "','"
					+ title
					+ "','"
					+ xml
					+ "','" + date + "' );";
			stmt.executeUpdate(sql);

			sql = "INSERT INTO APPLICATION_MAP (USERNAME, APPLICATION_ID) "
					+ "VALUES ('" + username + "','" + applicationID + "' );";
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			closeDatabase();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Retrieves pending applications for a specific user
	 * @param username
	 * @return All the pending applications for a specific user
	 */
	public List<Patent> getPendingApplications(String username) {
		Statement stmt, stmt2 = null;
		Patent patent = null;
		String applicationID;
		ResultSet rs2;
		List<Patent> pendingList = new ArrayList<Patent>();
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			stmt2 = c.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM APPLICATION_MAP where USERNAME = '"
							+ username + "';");
			while (rs.next()) {
				applicationID = rs.getString("APPLICATION_ID");
				rs2 = stmt2
						.executeQuery("SELECT * FROM APPLICATION where APPLICATION_ID = '"
								+ applicationID + "' AND STATUS = 'Pending';");
				while (rs2.next()) {
					patent = new Patent(rs2.getString("APPLICATION_ID"),
							username, rs2.getString("TITLE"),
							rs2.getString("STATUS"), rs2.getString("XML"),
							rs2.getString("SUBMITTED_DATE"),
							rs2.getString("PUBLISHED_DATE"),
							rs2.getString("MODIFIED_DATE"));
					pendingList.add(patent);
				}
			}
			rs.close();
			stmt.close();
			closeDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return pendingList;
	}

	/**
	 * Retrieves published patents for a specific user
	 * @param username
	 * @return Published patents for a specific user
	 */
	public List<Patent> getPublishedPatents(String username) {
		Statement stmt, stmt2 = null;
		Patent patent = null;
		String applicationID;
		ResultSet rs2;
		List<Patent> publishedList = new ArrayList<Patent>();
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			stmt2 = c.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM APPLICATION_MAP where USERNAME = '"
							+ username + "';");
			while (rs.next()) {
				applicationID = rs.getString("APPLICATION_ID");
				rs2 = stmt2
						.executeQuery("SELECT * FROM APPLICATION where APPLICATION_ID = '"
								+ applicationID + "' AND STATUS = 'Published';");
				while (rs2.next()) {
					patent = new Patent(rs2.getString("APPLICATION_ID"),
							username, rs2.getString("TITLE"),
							rs2.getString("STATUS"), rs2.getString("XML"),
							rs2.getString("SUBMITTED_DATE"),
							rs2.getString("PUBLISHED_DATE"),
							rs2.getString("MODIFIED_DATE"));
					publishedList.add(patent);
				}
			}
			rs.close();
			stmt.close();
			closeDatabase();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return publishedList;
	}

	/**
	 * Retrieves all the patents in the database
	 * @return List of all patents
	 */
	public List<Patent> getAllPatents() {
		List<Patent> patents = new ArrayList<Patent>();
		patents.addAll(getPatents(false));
		patents.addAll(getPatents(true));

		return patents;
	}

	public List<Patent> getPatents(boolean isPublished) {
		return getPatents(isPublished ? PatentStatus.PUBLISHED
				: PatentStatus.PENDING);
	}

	/**
	 * Retrieves patents according to status
	 * @param stat
	 * @return List of patents based on status
	 */
	public List<Patent> getPatents(PatentStatus stat) {
		Statement stmt = null;
		Patent patent = null;
		List<Patent> publishedList = new ArrayList<Patent>();

		String status = stat == PatentStatus.PUBLISHED ? "Published"
				: stat == PatentStatus.PENDING ? "Pending" : "Rejected";

		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT "
							+ "APPLICATION.APPLICATION_ID AS APPLICATION_ID,"
							+ "APPLICATION_MAP.USERNAME AS USERNAME,"
							+ "TITLE,"
							+ "STATUS,"
							+ "XML,"
							+ "SUBMITTED_DATE,"
							+ "PUBLISHED_DATE,"
							+ "MODIFIED_DATE "
							+ "FROM APPLICATION, APPLICATION_MAP "
							+ "WHERE APPLICATION.APPLICATION_ID = APPLICATION_MAP.APPLICATION_ID AND STATUS='"
							+ status + "';");

			while (rs.next()) {
				patent = new Patent(rs.getString("APPLICATION_ID"),
						rs.getString("USERNAME"), rs.getString("TITLE"),
						rs.getString("STATUS"), rs.getString("XML"),
						rs.getString("SUBMITTED_DATE"),
						rs.getString("PUBLISHED_DATE"),
						rs.getString("MODIFIED_DATE"));
				publishedList.add(patent);
			}
			rs.close();
			stmt.close();
			closeDatabase();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return publishedList;
	}

	/**
	 * Retrieves a patent object from the database
	 * @param id
	 * @return - a patent object
	 */
	public Patent getPatentByID(String id) {
		Statement stmt = null;
		Patent patent = null;
		String username = null;
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt
					.executeQuery("SELECT * FROM APPLICATION_MAP where APPLICATION_ID = '"
							+ id + "';");
			while (rs.next()) {
				username = rs.getString("USERNAME");
				rs.close();
				stmt.close();
			}
			rs = stmt
					.executeQuery("SELECT * FROM APPLICATION where APPLICATION_ID = '"
							+ id + "';");
			while (rs.next()) {
				patent = new Patent(rs.getString("APPLICATION_ID"), username,
						rs.getString("TITLE"), rs.getString("STATUS"),
						rs.getString("XML"), rs.getString("SUBMITTED_DATE"),
						rs.getString("PUBLISHED_DATE"),
						rs.getString("MODIFIED_DATE"));
			}
			rs.close();
			stmt.close();
			closeDatabase();
		} catch (Exception e) {
			return null;
		}
		return patent;
	}

	/**
	 * Changes patent status
	 * @param id
	 * @param stat
	 * @return true if status changed successfully
	 */
	public boolean updatePatent(String id, PatentStatus stat) {

		String status = stat == PatentStatus.PUBLISHED ? "Published"
				: stat == PatentStatus.PENDING ? "Pending" : "Rejected";

		Statement stmt = null;
		try {
			openDatabase();
			c.setAutoCommit(false);
			stmt = c.createStatement();
			String sql = "UPDATE APPLICATION set STATUS = '" + status
					+ "' where  APPLICATION_ID = '" + id + "'";
			stmt.executeUpdate(sql);
			c.commit();
			stmt.close();
			closeDatabase();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private boolean openDatabase() {
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + path);
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			e.printStackTrace();
			return false;
		}
		return true;
	}

	private static boolean closeDatabase() {
		try {
			c.close();
		} catch (Exception e) {
			System.err.println(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
		return true;
	}

	/**
	 * Generates random application ID
	 * @return random application identifier
	 */
	public String generateRandomApplicationID() {
		final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Random rnd = new Random();

		StringBuilder sb = new StringBuilder(7);
		for (int i = 0; i < 7; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
}