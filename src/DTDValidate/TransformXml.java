/*
 * Transform XML class by Chanthu
 */
package DTDValidate;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

public class TransformXml {

	public static final String failed = "<h1>Failed to transform file</h1>";
	private TransformerFactory tFactory;

	public TransformXml() {
		tFactory = TransformerFactory.newInstance();
	}

	/**
	 * Transforms an XML in the source provided to a different form by applying
	 * the provided XSL
	 * 
	 * @param sourcePath
	 * @param xsltPath
	 * @return the transformed {@link String}
	 */
	public String transform(String sourcePath, String xsltPath) {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		try {
			XMLReader reader = XMLReaderFactory.createXMLReader();
			reader.setEntityResolver(new EntityResolver() {

				/**
				 * Ignores the specified DTD to fix the transform from failing
				 * if the DTD file is not found
				 */
				@Override
				public InputSource resolveEntity(String publicId,
						String systemId) throws SAXException, IOException {
					if (systemId.endsWith(".dtd")) {
						StringReader stringInput = new StringReader(" ");
						return new InputSource(stringInput);
					} else {
						return null; // use default behavior
					}
				}
			});

			SAXSource xmlSource = new SAXSource(reader, new InputSource(
					new FileReader(new File(sourcePath))));

			Transformer transformer = tFactory.newTransformer(new StreamSource(
					new File(xsltPath)));

			// Byte stream to write into
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			transformer.transform(xmlSource, new StreamResult(baos));

			// Return the transformed string
			return new String(baos.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return failed;
	}

	public static void main(String[] args) {
		TransformXml trXml = new TransformXml();
		String transformed = trXml.transform(
				"src/../WebContent/samples/sample1/EPOSample1.xml",
				"src/../WebContent/samples/EPOXsl.xsl");

		if (!transformed.equals(failed)) {
			System.out.println("TEST 1 SUCCESS");
		}

		transformed = trXml.transform(
				"src/../WebContent/samples/sample2/EPOSample2.xml",
				"src/../WebContent/samples/EPOXsl.xsl");

		if (!transformed.equals(failed)) {
			System.out.println("TEST 2 SUCCESS");
		}
	}
}
