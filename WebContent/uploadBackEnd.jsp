<!-- Upload XML by Nizam  -->

<%@ page import="com.iide.patent.Database"%>
<%@ page import="java.io.*"%>
<%@ page import="org.w3c.dom.*"%>
<%@ page import="org.xml.sax.*"%>
<%@ page import="javax.xml.parsers.*"%>
<%@ page import="javax.xml.transform.dom.DOMSource"%>
<%@ page import="javax.xml.transform.stream.StreamResult"%>
<%@ page import="javax.xml.transform.*"%>

<%@ include file="header.jsp"%>
<%
	Database db = new Database(request);
%>

<%
	//name is the filename used for FolderName to be created
	String name = null;
	String a = null;
	String localPath = null;
	boolean succes = false;

	//Parse the XML file which been uploaded
	String saveFile = "";
	String contentType = request.getContentType();
	if ((contentType != null)
			&& (contentType.indexOf("multipart/form-data") >= 0)) {
		DataInputStream in = new DataInputStream(
				request.getInputStream());
		int formDataLength = request.getContentLength();
		byte dataBytes[] = new byte[formDataLength];
		int byteRead = 0;
		int totalBytesRead = 0;
		while (totalBytesRead < formDataLength) {
			byteRead = in.read(dataBytes, totalBytesRead,
					formDataLength);
			totalBytesRead += byteRead;
		}
		String file = new String(dataBytes);

		saveFile = file.substring(file.indexOf("filename=\"") + 10);

		saveFile = saveFile.substring(0, saveFile.indexOf("\n"));
		saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1,
				saveFile.indexOf("\""));
		name = saveFile;
		String fileName = name;
		name = name.substring(0, name.length() - 4);

		//a is the URL of the project been loaded so can store the xml file
		a = request.getServletContext().getRealPath("/IIDE Patent/");
		a = a.substring(0, a.length() - 11);
		a = a + "Patent";

		//if Patent folder doesnt exist it will create it
		File fileSaveDir = new File(a);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}

		name = db.generateRandomApplicationID();

		a = a + "/" + name;

		//if the xml been uploaded folder(same name as xml) doesnt exist it will create
		fileSaveDir = new File(a);
		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
		}

		//save the parsed xml file
		int lastIndex = contentType.lastIndexOf("=");
		String boundary = contentType.substring(lastIndex + 1,
				contentType.length());
		int pos;
		pos = file.indexOf("filename=\"");
		pos = file.indexOf("\n", pos) + 1;
		pos = file.indexOf("\n", pos) + 1;
		pos = file.indexOf("\n", pos) + 1;
		int boundaryLocation = file.indexOf(boundary, pos) - 4;
		int startPos = ((file.substring(0, pos)).getBytes()).length;
		int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
		saveFile = a + "/" + saveFile;
		File ff = new File(saveFile);
		FileOutputStream fileOut = new FileOutputStream(ff);
		fileOut.write(dataBytes, startPos, (endPos - startPos));
		fileOut.flush();
		fileOut.close();
%>

<div class="jumbotron">
	<div class="container">
		<h1>Apply</h1>
		<p>Patent application validation</p>
	</div>
</div>

<div class="container">
	<a href="applicant.jsp" class="btn btn-warning">Back to
		applications page</a><br />
	<br />
	<div class="row">
		<div class="col-md-6">

			<!--Validate XML file with epo v1 DTD-->
			<%
				try {
						DocumentBuilderFactory factory = DocumentBuilderFactory
								.newInstance();
						factory.setValidating(true);
						DocumentBuilder builder = factory.newDocumentBuilder();
						builder.setErrorHandler(new org.xml.sax.ErrorHandler() {
							//avoids any fatal errors
							public void fatalError(SAXParseException exception)
									throws SAXException {
							}

							//Display error when validate
							public void error(SAXParseException e)
									throws SAXParseException {
								System.out.println("Error at " + e.getLineNumber()
										+ " line.");
								System.out.println(e.getMessage());
							}

							//Print any error message
							public void warning(SAXParseException err)
									throws SAXParseException {
								System.out.println(err.getMessage());
							}
						});

						//Ignores any default DTD file or location of dtd in XML
						//overwrites it to use local DTD in WebContent folder
						builder.setEntityResolver(new EntityResolver() {
							@Override
							public InputSource resolveEntity(String publicId,
									String systemId) throws SAXException,
									IOException {

								if (systemId
										.contains("ep-patent-document-v1-0.dtd")) {
									return new InputSource(
											new FileReader(
													getServletContext()
															.getRealPath(
																	"/ep-patent-document-v1-0.dtd/")));
								} else {
									return null;
								}
							}
						});

						//Parse XML against DTD
						Document xmlDocument = builder.parse(new FileInputStream(
								saveFile));

						DOMSource source = new DOMSource(xmlDocument);
						StreamResult result = new StreamResult(System.out);

						out.println("<strong><p class=\"text-success\">XML validated successfully</p></strong>");
						out.println("Please enter a title for this patent");
						out.println();

						localPath = "/Patent/" + name + "/" + fileName;
						succes = true;

					} catch (Exception e) {

						out.println("<p class=\"text-danger\">XML failed to validate</p>");

						out.println("<p>");
						out.println(e.getMessage());
						out.println("</p>");
					}
			%>


			<%
				}
			%>


			<%
				if (succes) {
			%>


			<form action="PatentSubmit" method="post">
				<br /> <strong>Patent title:</strong> <input type="text" id="name"
					name="name" placeholder="" class="input-xlarge form-control"
					value="<%=name%>"> <input type="hidden" name="path"
					value="<%=localPath%>"> <br> <input type="submit"
					class="btn btn-success pull-right" value="Submit" />
			</form>

			<%
				}
			%>

			<div class="col-md-6"></div>
		</div>
	</div>
	<%@ include file="footer.jsp"%>