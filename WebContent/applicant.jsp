<!-- Applicant page by Andrew -->
<%@page import="com.iide.patent.Database"%>
<%@page import="com.iide.patent.Patent"%>
<%@page import="java.util.List"%>

<%@ include file="header.jsp"%>
<%
	if (sesObj.isLoggedIn()) {
		if (!sesObj.isApplciant()) {
			response.sendRedirect("home.jsp");
			return;
		}
	} else {
		response.sendRedirect("home.jsp");
		return;
	}

	Database db = new Database(request);
	List<Patent> pendingList = db.getPendingApplications(sesObj
			.getName());
	List<Patent> publishedList = db.getPublishedPatents(sesObj
			.getName());
%>

<div class="jumbotron">
	<div class="container">
		<h1>Dashboard</h1>
		<p>Apply for patents</p>
	</div>
</div>
<div class="container">
	<ul class="nav nav-tabs">
		<!--<li class="active"><a href="#something" data-toggle="tab">Something</a></li> -->
		<li class="active"><a href="#pending" data-toggle="tab">Pending
				applications <span class="badge"> <%
 	out.println(pendingList.size());
 %>
			</span>
		</a></li>
		<li><a href="#published" data-toggle="tab">Published patents
				<span class="badge"> <%
 	out.println(publishedList.size());
 %>
			</span>
		</a></li>
		<li><a href="#application" data-toggle="tab">Patent
				application </a></li>
	</ul>

	<div id="myTabContent" class="tab-content">
		<!-- 
		<div class="tab-pane fade active in" id="something">
			<p>Something...</p>
		</div>
 		-->
		<div class="tab-pane fade active in" id="pending">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th class="text-center">Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < pendingList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=pendingList.get(i).getId()%>">
								<%
									out.print(pendingList.get(i).getTitle());
								%>
						</a></td>
						<td>
							<%
								out.print(pendingList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(pendingList.get(i).getSubmittedDate());
							%>
						</td>
						<td class="text-center"><a
							href=" <%=homeUrlRelative%><%=pendingList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="published">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < publishedList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=publishedList.get(i).getId()%>">
								<%
									out.print(publishedList.get(i).getTitle());
								%>
						</a></td>
						<td>
							<%
								out.print(publishedList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(publishedList.get(i).getSubmittedDate());
							%>
						</td>
						<td><a
							href="<%=homeUrlRelative%><%=publishedList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
		<div class="tab-pane fade" id="application">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Submit a patent application</h3>
				</div>
				<div class="panel-body">
					<FORM ENCTYPE="multipart/form-data" ACTION="uploadBackEnd.jsp"
						METHOD=POST>
						<table>
							<tr>
								<td>Upload the patent file in xml format<br /> <br /></td>
							</tr>
							<tr>
								<td>Choose the file you would like to upload:</td>
							</tr>
							<tr>

								<td><input class="btn btn-warning" NAME="file" TYPE="file"></td>
							</tr>
							<tr>
								<td><input class="btn btn-success pull-right" type="submit"
									value="Send File"></td>
							</tr>
						</table>
					</FORM>
					<br /> <br />
					<p>You will be required to enter a title for the patent once
						your file has been validated with the EPO DTD</p>
				</div>
			</div>
		</div>
	</div>
</div>

<%@ include file="footer.jsp"%>