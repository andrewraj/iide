<!-- Header by Andrew & Chanthu -->
<%@page import="com.iide.patent.SessionObject"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
	SessionObject sesObj = SessionObject.get(request);
	String homeUrlRelative = request.getContextPath();
	String homeUrlAbs = request.getServletContext().getRealPath("/");
	String pageUrlRel = request.getRequestURI();
%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<!-- Not available yet -->
<link rel="shortcut icon" href="images/favicon.ico">

<title>IIDE Patent System</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/jumbotron.css" rel="stylesheet">
<link href="css/custom.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand " href="home.jsp"><span class="text-default">IIDE Patent System</span></a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<%
						if (sesObj.isLoggedIn() && sesObj.isApplciant()) {
					%>
					<li><a href="applicant.jsp">Applicant Dashboard</a></li>
					<%
						} else if (sesObj.isLoggedIn()) {
					%>
					<li><a href="reviewer.jsp">Reviewer Dashboard</a></li>
					<%
						}
					%>

					<li><a href="viewPatents.jsp">View Patents</a></li>
				</ul>
				<div class="navbar-btn navbar-right">
					<%
						if (!sesObj.isLoggedIn()) {
					%>
					<a data-toggle="modal" href="#myModal" class="btn btn-primary btn">Sign
						in</a>
					<%
						} else {
					%>
					<!-- 
					<form class="form-horizontal" action="SigninSignoutServlet"
						method="POST">
						<label class="text-muted"> <%=sesObj.getName()%></label> 
						<input type="hidden" name="type" value="logout">
						<button type="submit" class="btn btn-primary">Sign Out</button>
					</form>
					<!-- Begin logged user dropdown -->
					<div class="pull-right">
						<ul class="nav">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown">Signed in as <%=sesObj.getName()%> <b
									class="caret"></b></a>
								<ul class="dropdown-menu">
									<li><a href="home.jsp"><span
											class="glyphicon glyphicon-user"></span>Preferences</a></li>
									<li><p class="divider"></p></li>
									<li><a
										href="<%=homeUrlRelative%>/SigninSignoutServlet?type=logout&returnUrl=<%=request.getRequestURL()%>"
										type="submit"> <span class="glyphicon glyphicon-off"></span>
											Logout
									</a></li>
								</ul></li>
						</ul>
					</div>

					<!-- End logged user dropdown -->

					<%
						}
					%>

				</div>
			</div>
			<!--/.navbar-collapse -->
		</div>
	</div>


	<div class="modal fade" id="error" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Error</h4>
				</div>
				<div class="modal-body">
					<p class="text-center text-danger">
						<%=request.getParameter("error")%>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>


	<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Sign in to IIDE Patent System</h4>
				</div>
				<div class="modal-body">
					<!-- Sign up form -->
					<form class="form-horizontal" action="SigninSignoutServlet"
						method="POST">
						<fieldset>
							<div class="control-group">
								<!-- Username -->
								<label class="control-label" for="username">Username</label>
								<div class="controls">
									<input type="text" id="username" name="username" placeholder=""
										class="input-xlarge form-control">
								</div>
							</div>

							<div class="control-group">
								<!-- Password-->
								<label class="control-label" for="password">Password</label>
								<div class="controls">
									<input type="password" id="password" name="password"
										placeholder="" class="input-xlarge form-control">
								</div>
							</div>

							<input type="hidden" name="type" value="login"> <input
								type="hidden" name="returnUrl"
								value="<%=request.getRequestURL()%>">

						</fieldset>

						<div class="modal-footer">
							<p class="text-left">
								New user? Click <strong><a data-dismiss="modal"
									data-toggle="modal" href="#signUpModal">here</a></strong> to register
							</p>
							<button type="button" class="btn btn-default"
								data-dismiss="modal">Cancel</button>
							<button type="submit" class="btn btn-primary">Sign in</button>
						</div>
					</form>
					<!-- Sign up form end -->
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->

	<!-- Modal -->
	<div class="modal fade" id="signUpModal" tabindex="-1" role="dialog"
		aria-labelledby="signUpModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">New to IIDE Patent System? Sign up!</h4>
				</div>
				<form accept-charset="UTF-8" action="SigninSignoutServlet"
					method="post">
					<div class="modal-body">
						<!-- Sign up form -->
						<div class="span3">
							<fieldset>
								<div class="control-group">
									<!-- First name -->
									<label class="control-label" for="firstname">First Name</label>
									<div class="controls">
										<input class="span3 form-control" name="firstname"
											id="firstname" placeholder="First Name" type="text">
									</div>
								</div>

								<div class="control-group">
									<!-- Last name -->
									<label class="control-label" for="lastname">Last Name</label>
									<div class="controls">
										<input class="span3 form-control" name="lastname"
											id="lastname" placeholder="Last Name" type="text">
									</div>
								</div>

								<div class="control-group">
									<!-- E-Mail-->
									<label class="control-label" for="email">E-Mail</label>
									<div class="controls">
										<input class="span3 form-control" name="email" id="email"
											placeholder="E-Mail" type="email">
									</div>
								</div>

								<div class="control-group">
									<!-- Username -->
									<label class="control-label" for="username">Username</label>
									<div class="controls">

										<input class="span3 form-control" name="username"
											id="username" placeholder="Username" type="text">
									</div>
								</div>

								<div class="control-group">
									<!-- Username -->
									<label class="control-label" for="password">Password</label>
									<div class="controls">

										<input class="span3 form-control" name="password"
											id="password" placeholder="Password" type="password">
									</div>
								</div>
							</fieldset>
						</div>
						<input type="hidden" name="type" value="signup"> <input
							type="hidden" name="returnUrl"
							value="<%=request.getRequestURL()%>">
						<!-- Sign up form end -->
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-primary">Sign up</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->