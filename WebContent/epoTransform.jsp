<!-- EPO Transform by Chanthu -->
<%@page import="com.iide.patent.Database"%>
<%@page import="DTDValidate.TransformXml"%>

<%@ include file="header.jsp"%>

<%
	Database db = new Database(request);

	String xslPath = homeUrlAbs + "/samples/EPOXsl.xsl";
	String xmlPath = homeUrlAbs;

	String appId = request.getParameter("appId");
	if (appId != null && !appId.equals("")) {
		xmlPath += db.getPatentByID(appId).getXml();
	}
%>

<div class="jumbotron">
	<div class="container">
		<h1>Patent Viewer</h1>
		<p>Displays patent as html</p>
	</div>
</div>
<div class="container">
	<%
		out.write(new TransformXml().transform(xmlPath, xslPath));
	%>

</div>

<%@ include file="footer.jsp"%>