<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0">

	<xsl:output method="html" indent="yes" />
	<xsl:param name="path" />

	<xsl:template match="ep-patent-document">
		<!-- Parent meta-data and applicants -->
		<div>
			<div class="text-center" style="margin-top:25px;">
				<h4>
					<xsl:value-of select="SDOBI/B100/B120/B121" />
					<xsl:text> (</xsl:text>
					<xsl:value-of select="./@id" />
					<xsl:text>)</xsl:text>
				</h4>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-6">
					<xsl:apply-templates select="SDOBI/B200" />
				</div>
				<div class="col-md-6">
					<xsl:apply-templates select="SDOBI/B400" />
					<xsl:apply-templates select="SDOBI/B300" />
				</div>
			</div>
			<hr />
			<div class="row">
				<div class="col-md-6">
					<xsl:apply-templates select="SDOBI/B700/B710" />
					<xsl:apply-templates select="SDOBI/B700/B740" />
				</div>
				<div class="col-md-6">
					<xsl:apply-templates select="SDOBI/B700/B720" />
				</div>
			</div>
			<hr />
			<div>
				<xsl:apply-templates select="SDOBI/B500/B540" />
			</div>
			<xsl:apply-templates />
		</div>
	</xsl:template>

	<xsl:template match="SDOBI/B500/B540">
		<xsl:for-each select="./B541">
			<xsl:if test=".='en'">
				<h2 class="text-center">
					<xsl:value-of select="./following-sibling::B542" />
				</h2>
			</xsl:if>
		</xsl:for-each>
	</xsl:template>

	<!-- Application filing details -->
	<xsl:template match="SDOBI/B200">
		<div>
			<strong>Application Number: </strong>
			<xsl:value-of select="B210" />
		</div>
		<div>
			<strong>Date Of Filing: </strong>
			<xsl:value-of select="B220/date" />
		</div>
	</xsl:template>

	<!-- Application publication details -->
	<xsl:template match="SDOBI/B400">
		<div>
			<strong>Date Of Publication: </strong>
			<xsl:value-of select="B405/date" />
		</div>
	</xsl:template>

	<!-- Application priority details -->
	<xsl:template match="SDOBI/B300">
		<div>
			<strong>Priority: </strong>
			<xsl:value-of select="B320/date" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="B330/ctry" />
			<xsl:text> </xsl:text>
			<xsl:value-of select="B310" />
		</div>
	</xsl:template>

	<!-- Applicants details -->
	<xsl:template match="SDOBI/B700/B710">
		<h4 class="text-primary">Applicants: </h4>
		<xsl:for-each select="./B711">
			<xsl:call-template name="processAddress" />
		</xsl:for-each>
	</xsl:template>

	<!-- Investors details -->
	<xsl:template match="SDOBI/B700/B720">
		<h4 class="text-primary">Inventors: </h4>
		<xsl:for-each select="./B721">
			<xsl:call-template name="processAddress" />
		</xsl:for-each>
	</xsl:template>

	<!-- Representatives details -->
	<xsl:template match="SDOBI/B700/B740">
		<h4 class="text-primary">Representatives: </h4>
		<xsl:for-each select="./B741">
			<xsl:call-template name="processAddress" />
		</xsl:for-each>
	</xsl:template>

	<!-- Processes address tag -->
	<xsl:template name="processAddress">
		<address>
			<strong>
				<xsl:value-of select="./snm" />
			</strong>
			<br />
			<xsl:value-of select="./adr/str" />
			<br />
			<xsl:value-of select="./adr/city" />
			<xsl:text> (</xsl:text>
			<xsl:value-of select="./adr/ctry" />
			<xsl:text>)</xsl:text>
		</address>
	</xsl:template>

	<!-- Abstract, Description, Drawings -->
	<xsl:template
		match="abstract | description | drawings | claims | search-report-data">
		<h3>
			<xsl:call-template name="UpperFirst">
				<xsl:with-param name="tx" select="local-name()" />
			</xsl:call-template>
		</h3>

		<div>
			<xsl:choose>
				<xsl:when test="local-name() = 'claims'">
					<ol>
						<xsl:apply-templates select="descendant::node()" />
					</ol>
				</xsl:when>
				<xsl:otherwise>
					<xsl:apply-templates select="descendant::node()" />
				</xsl:otherwise>
			</xsl:choose>
		</div>
	</xsl:template>

	<xsl:template match="claim">
		<ul style="	display: list-item; list-style: decimal;">
			<xsl:value-of select="claim-text"
				disable-output-escaping="yes"></xsl:value-of>
			<br />
			<br />
		</ul>
	</xsl:template>

	<!-- Headings -->
	<xsl:template match="heading">
		<h4 class="text-primary">
			<xsl:value-of select="." />
		</h4>
	</xsl:template>

	<!-- Paragraphs -->
	<xsl:template match="p">
		<p>
			<xsl:value-of select="." disable-output-escaping="yes" />
		</p>
	</xsl:template>

	<!-- Bold blocks -->
	<xsl:template match="b--">
		<xsl:if test="name(..) != 'heading'">
			<strong>
				<xsl:value-of select="./text()" />
			</strong>
		</xsl:if>
	</xsl:template>

	<!-- Tables -->
	<xsl:template match="tables">
		<xsl:for-each select="table">
			<table class="table">
				<xsl:for-each select="tgroup">
					<xsl:for-each select="tbody">
						<xsl:for-each select="row">
							<tr>
								<xsl:for-each select="entry">
									<td>
										<xsl:value-of select="./text()"></xsl:value-of>
									</td>
								</xsl:for-each>
							</tr>
						</xsl:for-each>
					</xsl:for-each>
				</xsl:for-each>
			</table>
		</xsl:for-each>
	</xsl:template>

	<!-- Images --> <!-- "http://www.ausaid.gov.au/budget/budget05/images/diagram3.gif" -->
	<xsl:template match="img | doc-page">
		<div class="text-center">
			<img>
				<xsl:attribute name="src">
     				<xsl:value-of select="$path" /><xsl:value-of
					select="./@file" />
  				</xsl:attribute>
			</img>
			<br />
			<span>
				<strong>Figure Id: </strong>
				<xsl:value-of select="./@id" />
			</span>
			<br />
			<br />
		</div>
	</xsl:template>

	<!-- Lists (Both ordered and unordered) -->
	<xsl:template match="ol | ul">
		<ol>
			<xsl:for-each select="li">
				<li>
					<xsl:value-of select="." disable-output-escaping="yes" />
				</li>
				<br />
			</xsl:for-each>
		</ol>
	</xsl:template>

	<xsl:template match="*">
		<!--<xsl:value-of select="local-name()"></xsl:value-of> -->
	</xsl:template>

	<!-- FUNCTIONS -->
	<xsl:variable name="vLower" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="vUpper" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />

	<!-- Convert first letter to upper case -->
	<xsl:template name="UpperFirst">
		<xsl:param name="tx" />
		<xsl:value-of
			select="concat(translate(substring($tx,1,1), $vLower, $vUpper), 
					substring($tx, 2), 
					substring(' ', 1 div not(position()=last())))" />
	</xsl:template>
</xsl:stylesheet>