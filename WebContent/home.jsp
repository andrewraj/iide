<!-- Home page by Andrew -->
<%@ include file="header.jsp"%>

<!-- Sample of how to write a page using the existing template -->
<!-- Just include the header & footer directive and everything that falls into the body section  -->

<div class="jumbotron">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<img src="images/icon.png" class="img-responsive">
			</div>
			<div class="col-md-6">

				<h1>IIDE Patent System</h1>
				<p>A responsive patent system that is available for all devices</p>
			</div>
		</div>


	</div>
</div>

<div class="container">

	<div class="panel panel-primary">
		<div class="panel-heading">
			<h2 class="panel-title">Home</h2>
		</div>
		<blockquote>
			<p>Protect your invention with IIDE Patenting System.</p>
		</blockquote>

		<ul class="list-group">
			<li class="list-group-item active">Features</li>
			<li class="list-group-item">Patent Viewer</li>
			<li class="list-group-item">Download original published XML Patents</li>
			<li class="list-group-item">Online Patent application</li>
			<li class="list-group-item">Responsive design</li>
			<li class="list-group-item">Mobile friendly</li>
			<li class="list-group-item">EPO Validation</li>
		</ul>

		<div class="panel-footer"></div>
	</div>
</div>


<%@ include file="footer.jsp"%>
