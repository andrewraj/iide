<!-- Reviewer page by Andrew & Chanthu -->
<%@page import="com.iide.patent.Database"%>
<%@page import="com.iide.patent.Patent"%>
<%@page import="com.iide.patent.PatentStatus"%>
<%@page import="java.util.List"%>

<%@ include file="header.jsp"%>
<%
	if (sesObj.isLoggedIn()) {
		if (sesObj.isApplciant()) {
			response.sendRedirect("home.jsp");
			return;
		}
	} else {
		response.sendRedirect("home.jsp");
		return;
	}

	Database db = new Database(request);
	List<Patent> pendingList = db.getPatents(PatentStatus.PENDING);
	List<Patent> publishedList = db.getPatents(PatentStatus.PUBLISHED);
	List<Patent> rejectedList = db.getPatents(PatentStatus.REJECTED);
%>

<div class="jumbotron">
	<div class="container">
		<h1>Reviewer</h1>
		<p>Manage and publish patents</p>
	</div>
</div>
<div class="container">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#pending" data-toggle="tab">Pending
				applications <span class="badge"> <%
 	out.println(pendingList.size());
 %>
			</span>
		</a></li>
		<li><a href="#published" data-toggle="tab">Published patents
				<span class="badge"> <%
 	out.println(publishedList.size());
 %>
			</span>
		</a></li>

		<li><a href="#rejected" data-toggle="tab">Rejected patents <span
				class="badge"> <%
 	out.println(rejectedList.size());
 %>
			</span>
		</a></li>

	</ul>

	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade active in" id="pending">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th>Status</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < pendingList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=pendingList.get(i).getId()%>">
								<%
									out.print(pendingList.get(i).getTitle());
								%>
						</a> <br />
							<p>
								Username:
								<%
								out.print(pendingList.get(i).getUsername());
							%>
							</p></td>
						<td>
							<%
								out.print(pendingList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(pendingList.get(i).getSubmittedDate());
							%>
						</td>
						<td>
							<div class="btn-group">
								<button type="button" class="btn btn-default dropdown-toggle"
									data-toggle="dropdown">
									<%
										out.print(pendingList.get(i).getStatus());
									%>
									<span class="caret"></span>
								</button>
								<ul class="dropdown-menu" role="menu">
									<li><a
										href="<%=homeUrlRelative%>/ChangeStatus?status=publish&id=<%=pendingList.get(i).getId()%>"
										type="submit">Publish</a></li>
									<li><a
										href="<%=homeUrlRelative%>/ChangeStatus?status=reject&id=<%=pendingList.get(i).getId()%>"
										type="submit">Reject</a></li>
								</ul>
							</div>
						</td>
						<td><a
							href="<%=homeUrlRelative%><%=pendingList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="published">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < publishedList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=publishedList.get(i).getId()%>">
								<%
									out.print(publishedList.get(i).getTitle());
								%>
						</a></td>
						<td>
							<%
								out.print(publishedList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(publishedList.get(i).getSubmittedDate());
							%>
						</td>
						<td><a
							href="<%=homeUrlRelative%><%=publishedList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>

		<div class="tab-pane fade" id="rejected">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < rejectedList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=rejectedList.get(i).getId()%>">
								<%
									out.print(rejectedList.get(i).getTitle());
								%>
						</a></td>
						<td>
							<%
								out.print(rejectedList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(rejectedList.get(i).getSubmittedDate());
							%>
						</td>
						<td><a
							href="<%=homeUrlRelative%><%=rejectedList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>

	</div>
</div>


<%@ include file="footer.jsp"%>