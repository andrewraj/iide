<!-- Footer by Andrew -->
<div class="container">
	<hr>
	<footer>
		<p>&copy; IIDE Patent System 2013</p>
	</footer>
</div>
<!-- /container -->


<!-- Bootstrap core JavaScript
    ================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>


<%
	String err = request.getParameter("error");
	if (err != null && !err.equals("")) {
%>

<script type="text/javascript">
	$(window).load(function() {
		$('#error').modal('show');
	});
</script>

<%
	}
%>
</body>
</html>