<!-- View patent page by Chanthu -->
<%@page import="com.iide.patent.Database"%>
<%@page import="com.iide.patent.Patent"%>
<%@page import="java.util.List"%>

<%@ include file="header.jsp"%>
<%
	Database db = new Database(request);
	List<Patent> publishedList = db.getPatents(true);
%>

<div class="jumbotron">
	<div class="container">
		<h1>Patents</h1>
		<p>View all published patents</p>
	</div>
</div>
<div class="container">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Published patents</h3>
		</div>
		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>Title</th>
						<th>Application ID</th>
						<th>Submitted Date</th>
						<th class="text-center">Download</th>
					</tr>
				</thead>
				<tbody>
					<%
						for (int i = 0; i < publishedList.size(); i++) {
					%>
					<tr>
						<td>
							<%
								out.print(i + 1);
							%>
						</td>
						<td><a
							href="epoTransform.jsp?appId=<%=publishedList.get(i).getId()%>">
								<%
									out.print(publishedList.get(i).getTitle());
								%>
						</a></td>
						<td>
							<%
								out.print(publishedList.get(i).getId());
							%>
						</td>
						<td>
							<%
								out.print(publishedList.get(i).getSubmittedDate());
							%>
						</td>
						<td class="text-center"><a
							href="<%=homeUrlRelative%><%=publishedList.get(i).getXml()%> ">
								<span class="glyphicon glyphicon-download"></span>
						</a></td>
					</tr>
					<%
						}
					%>
				</tbody>
			</table>
		</div>
	</div>
</div>

<%@ include file="footer.jsp"%>