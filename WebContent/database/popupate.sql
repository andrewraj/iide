
-- Table: ROLE
CREATE TABLE ROLE ( 
    ROLE_ID   TEXT PRIMARY KEY
                   NOT NULL
                   UNIQUE,
    ROLE_NAME TEXT NOT NULL 
);

INSERT INTO [ROLE] ([ROLE_ID], [ROLE_NAME]) VALUES ('ap', 'Applicant');
INSERT INTO [ROLE] ([ROLE_ID], [ROLE_NAME]) VALUES ('re', 'Reviewer');

-- Table: APPLICATION
CREATE TABLE APPLICATION ( 
    APPLICATION_ID TEXT PRIMARY KEY
                        NOT NULL
                        UNIQUE,
    TITLE          TEXT NOT NULL
                        DEFAULT ( 'No Title' ),
    STATUS         TEXT NOT NULL
                        DEFAULT ( 'Pending' ),
    XML            TEXT NOT NULL,
    SUBMITTED_DATE DATE NOT NULL,
    PUBLISHED_DATE DATE,
    MODIFIED_DATE  DATE 
);

INSERT INTO [APPLICATION] ([APPLICATION_ID], [TITLE], [STATUS], [XML], [SUBMITTED_DATE], [PUBLISHED_DATE], [MODIFIED_DATE]) VALUES ('Y45IODJ', 'Image Forming method', 'Pending', '/samples/sample1/EPOSample1.xml', '2013-10-09', null, null);
INSERT INTO [APPLICATION] ([APPLICATION_ID], [TITLE], [STATUS], [XML], [SUBMITTED_DATE], [PUBLISHED_DATE], [MODIFIED_DATE]) VALUES ('LMHC50H', 'Etrazole deritives', 'Pending', '/samples/sample2/EPOSample2.xml', '2013-10-09', null, null);

-- Table: USER
CREATE TABLE USER ( 
    USERNAME   TEXT PRIMARY KEY
                    NOT NULL ON CONFLICT FAIL
                    UNIQUE ON CONFLICT FAIL,
    PASSWORD   TEXT NOT NULL ON CONFLICT FAIL,
    EMAIL      TEXT NOT NULL ON CONFLICT FAIL
                    UNIQUE ON CONFLICT FAIL,
    FIRST_NAME TEXT NOT NULL,
    LAST_NAME  TEXT NOT NULL,
    ROLE_ID    TEXT DEFAULT ( 'ap' ) 
                    REFERENCES ROLE ( ROLE_ID ) 
);

INSERT INTO [USER] ([USERNAME], [PASSWORD], [EMAIL], [FIRST_NAME], [LAST_NAME], [ROLE_ID]) VALUES ('adwraj', 'ecb97ffafc1798cd2f67fcbc37226761', 'me@andrewraj.com', 'Andrew', 'Raj', 'ap');
INSERT INTO [USER] ([USERNAME], [PASSWORD], [EMAIL], [FIRST_NAME], [LAST_NAME], [ROLE_ID]) VALUES ('test', '098f6bcd4621d373cade4e832627b4f6', 'test@test.com', 'Testing', 'One', 're');

-- Table: APPLICATION_MAP
CREATE TABLE APPLICATION_MAP ( 
    USERNAME       TEXT REFERENCES USER ( USERNAME ),
    APPLICATION_ID TEXT REFERENCES APPLICATION ( APPLICATION_ID ) 
);

INSERT INTO [APPLICATION_MAP] ([USERNAME], [APPLICATION_ID]) VALUES ('adwraj', 'Y45IODJ');
INSERT INTO [APPLICATION_MAP] ([USERNAME], [APPLICATION_ID]) VALUES ('adwraj', 'LMHC50H');
