IIDE Assignment
---------------

When it's a group assignment, we really work as a group!!
This is our GIT repository
https://bitbucket.org/andrewraj/iide

Please see wiki for setting up details.
https://bitbucket.org/andrewraj/iide/wiki

All our project files, documentation is in there.
Please do check the site before marking our assignment as 
all relevant details including setting up is in the site.
------------------------------------------------------------------------------

Name: Mohamed Nizamudin Bin Nazar Mohamed
ID: s3223615

My part was part(a) Validate XML against DTD.

To complete my part,I worked on applicant.jsp and uploadBackEnd.jsp. 
Inside application.jsp user selects and uploads an XML file.
Once uploaded it will call uploadBackEnd where it parse the XML 
and save it in project folder. Once saved it will validate against EPO v1
DTD once validation veried it will save to database.

------------------------------------------------------------------------------

Name: Chanthu Thayyilkariyil Shamsu
ID:s3235256

My part was part(b) Transform the xml to html
To complete my part,I worked on TransformXml.java and epoTransform.jsp.
epoTransform.jsp gets an xml from using passed in ID and translates it to
html. This can be tested by clicking on any of the listed patent titles
in viewPatents.jsp

------------------------------------------------------------------------------

Name: Andrew Raj Paul Durai
ID: s3418631

My part was part(c) and I worked on the database file and database class for the
whole system. I have also worked on including a responsive design(bootstrap) and
a mobile friendly design. I have inntegrated both my other members work in a single
design. The whole application can be exported and tested by deploying it to a 
Tomcat server. The database class was tested with a main class within the server
but later removed for deployment purposes. The testing evidence can be found from
the git reposity from the previous commits.

Files I mainly worked on:-
-------------------------
Database.java
Patent.java
AESencrp.java
MD5Hashing.java
ChangeStatus.java
applicant.jsp
reviewer.jsp
header.jsp/footer.jsp
home.jsp

------------------------------------------------------------------------------

NOTE: Other than the files listed under each team members, name, all of us 
worked on most of the files and contributed to the project equally. The name
of the contributors of each file has been listed at the top of each file.

------------------------------------------------------------------------------

HOW TO RUN

Import the submitted IIDE Patent eclipse project into eclipse.
Right click and click run on Server.
Make sure the Tomcat server is name Apache Tomcat v7.0 IIDE as shown in 

https://bitbucket.org/andrewraj/iide/wiki/Set%20up%20Apache%20Tomcat%20to%20work%20with%20Eclispse

OR

Deploy the submitted IIDE Patent.war on tomcat

------------------------------------------------------------------------------

Patent Applicant Details:

Username: applicant
Password: applicant

Patent Reviewer Details:

Username: reviewer
Password: reviewer